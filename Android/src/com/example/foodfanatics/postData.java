package com.example.foodfanatics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class postData {
		
	public postData() {
	}

	//Send request to django db to retrieve all events
    public void postEvents(JSONObject eventJSON) throws IOException, JSONException
    {
    	BufferedReader reader=null;
    	String text = "";
     // Send data 
        try
        { 
          
         // Defined URL  where to send data
         URL url = new URL("http://radiant-chamber-5229.herokuapp.com/food/request_events/");
             
         // Send POST data request

         HttpURLConnection conn = (HttpURLConnection) url.openConnection();
         conn.setDoOutput(true); 
         OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
          wr.write( eventJSON.toString() ); 
          wr.flush(); 
      
          // Get the server response 
           
        reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String line = null;
        
        // Read Server Response
        while((line = reader.readLine()) != null)
            {
                   // Append server response in string
                   sb.append(line + "\n");
            }
            text = sb.toString();
        }
        catch(Exception ex)
        {
             
        }
        finally
        {
            try
            {
 
                reader.close();
            }

            catch(Exception ex) {}
        }
    }
	
}
