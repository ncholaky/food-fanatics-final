package com.example.foodfanatics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.dbtest.Events;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.Toast;

public class MainActivity extends Activity {
	private dbHelp db;
	int count = 0;
	boolean threadCount = true;
	ExpandableListAdapter listA;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
	
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = dbHelp.getInstance(this);
        if(threadCount)new RequestData(this).execute();
        threadCount = false;
        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.expandableListView1);
     //   db.deleteAll();
        // preparing list data
        prepareListData();
 
        listA = new ListAdapter(this, listDataHeader, listDataChild);
 
        // setting list adapter
        expListView.setAdapter(listA);
 
        // Listview Group click listener
        expListView.setOnGroupClickListener(new OnGroupClickListener() {
 
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                    int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();
                return false;
            }
        });
 
        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new OnGroupExpandListener() {
 
            @Override
            public void onGroupExpand(int groupPosition) {
              /*  Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();*/
            }
        });
 
        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {
 
            @Override
            public void onGroupCollapse(int groupPosition) {
               /* Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();*/
 
            }
        });
 
        // Listview on child click listener
        expListView.setOnChildClickListener(new OnChildClickListener() {
 
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                    int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                /*Toast.makeText(
                        getApplicationContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                        listDataHeader.get(groupPosition)).get(
                                        childPosition), Toast.LENGTH_SHORT)
                        .show();*/
                return false;
            }
        });
    }
 
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
 
      
        List<Events> Eve = db.getAllEvents();
        
//        for (int i =0 ;i < Eve.size(); i++){
//        	System.out.println(Eve.get(i));
//        }
        
        for (Events cn : Eve) {
        	String name = cn.getName().toString();
        	String free = "";
        	if(cn.getFree().equals("true"))
        		free = "Free Food";
        	else
        		free = "Selling Food";
        	List<String> setter = new ArrayList<String>();
        	listDataHeader.add("Event: " + cn.getName() + " - " + free + 
        			"\nDate: " + cn.getDate() + " Time: " + cn.getTime());
            setter.add(Html.fromHtml("<b>Location</b>: ") + cn.getLocation());
          setter.add("Organization: " + cn.getOrg());
          setter.add("Details: " + cn.getDetails());
          setter.add("Phone: " + cn.getPhoneNumber());
          setter.add("Email: " + cn.getEmail());
          setter.add("Link: " + cn.getLink());
            listDataChild.put(listDataHeader.get(count), setter); // Header, Child data
          count++;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    
    // Send request to django db to retrieve all events
    public String requestEvents(String stringUrl) throws IOException, JSONException
    {
    	JSONObject eventJSON;
        StringBuilder response  = new StringBuilder();
        System.out.println("requesting");
        URL url = new URL(stringUrl);
        System.out.println(url);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        System.out.println(urlConnection.getResponseCode());

        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
        {
        	System.out.println("connected");
            BufferedReader input = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()),8192);
            String strLine = null;
            while ((strLine = input.readLine()) != null)
            {
            	JSONArray arr = new JSONArray(strLine);
            	 for(int i = 0; i < arr.length(); i++){

                     eventJSON = arr.getJSONObject(i); 
                     db.addEvents(eventJSON);
                     //System.out.println("JSON: " +i+ " " + c.get("free"));
                 }
       
                response.append(strLine);
            }
            input.close();
        }

        return response.toString();
    }
    
    public void openForm(View view){
    	Intent getFormView = new Intent("com.example.foodfanatics.FORMACTIVITY");
		startActivity(getFormView);	
    }
    
    class RequestData extends AsyncTask<Void, Void, String> {
    	
    	MainActivity main;

    	public RequestData(MainActivity main) {
    		this.main = main;
    	}
    	
		protected String doInBackground(Void... params) {
			String eventList = "http://radiant-chamber-5229.herokuapp.com/food/request_events/?format=json";
	        System.out.println(eventList);
	        try {
	        	String response = main.requestEvents(eventList);
	        	return response;
	        }
	        catch (Exception e) {
	        	System.out.println("DIDN'T WORK");
	        	return null;
	        }
	        
		}
		
//		protected void onPostExecute(String str) {
//			
//			System.out.println(str);
//		}
    	
    }
   
}
