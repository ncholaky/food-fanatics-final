package com.example.foodfanatics;

import org.json.JSONObject;

import com.example.foodfanatics.R;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;

public class DisplayMessageActivity extends Activity {
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 // Get the message from the intent
	    Intent intent = getIntent();
	    String org = intent.getStringExtra(FormActivity.EXTRA_ORG);
	    String name = intent.getStringExtra(FormActivity.EXTRA_NAME);
	    String date = intent.getStringExtra(FormActivity.EXTRA_DATE);
	    String time = intent.getStringExtra(FormActivity.EXTRA_TIME);
	    String location = intent.getStringExtra(FormActivity.EXTRA_LOCATION);
	    String detail = intent.getStringExtra(FormActivity.EXTRA_DETAIL);
	    String free = intent.getStringExtra(FormActivity.EXTRA_FREE);
	    String phone = intent.getStringExtra(FormActivity.EXTRA_PHONE);
	    String email = intent.getStringExtra(FormActivity.EXTRA_EMAIL);
	    String link = intent.getStringExtra(FormActivity.EXTRA_LINK);
	    
	    
	    JSONObject event = new JSONObject();
	    try{
		    event.put("org", org);
		    event.put("event_name", name);
		    event.put("event_date", date);
		    event.put("event_time", time);
		    event.put("location", location);
		    event.put("detail", detail);
		    event.put("free", free);
		    event.put("phone_number", phone);
		    event.put("email", email);
		    event.put("link", link);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    
	    
	  
	 //////////   
	    setContentView(R.layout.activity_display_form);
		// Make sure we're running on Honeycomb or higher to use ActionBar APIs
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
	    
	    // Create the text view
        
        String total = ("Org Name is: " + org + "\n" + 
        				"Event Name is: " + name + "\n" +
        				"Date: " + date + "\n" + 
        				"Time is: " + time + "\n" +
        				"Location is: " + location + "\n" + 
        				"Description of Event: " + detail + "\n" +
        				"Is if Free? " + free + "\n" + 
        				"Contact Number is: " + phone + "\n" +
        				"Contact Email is:  " + email + "\n" + 
        				"Link for Event: " + link + "\n" );
        				
	    TextView textView = new TextView(this);
	    textView.setTextSize(15);
	    textView.setText(total);
	    
	   
	    
	    

	    // Set the text view as the activity layout
	    setContentView(textView);
		
		
        
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

/*
	@Override

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.display_message, menu);
		return true;
	}
	
	*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
