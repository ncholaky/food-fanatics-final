package com.example.foodfanatics;

import com.example.foodfanatics.R;

import android.os.Bundle;

import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;

public class FormActivity extends Activity {
	private dbHelp db;
	 public final static String EXTRA_ORG = "com.examle.foodfanatics.ORG";
	 public final static String EXTRA_NAME = "com.example.foodfanatics.NAME";
	 public final static String EXTRA_DATE = "com.examle.foodfanatics.DATE";
	 public final static String EXTRA_TIME = "com.examle.foodfanatics.TIME";
	 public final static String EXTRA_LOCATION = "com.examle.foodfanatics.LOCATION";
	 public final static String EXTRA_DETAIL = "com.examle.foodfanatics.DETAIL";
	 public final static String EXTRA_FREE = "com.examle.foodfanatics.FREE";
	 public final static String EXTRA_PHONE = "com.examle.foodfanatics.PHONE";
	 public final static String EXTRA_EMAIL= "com.examle.foodfanatics.EMAIL";
	 public final static String EXTRA_LINK = "com.examle.foodfanatics.LINK";
	 

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        db = dbHelp.getInstance(this);
        EditText editOrg = (EditText) findViewById(R.id.edit_org);
        editOrg.setFocusable(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
        
		
    }
    
    /** Called when the user clicks the Send button */
    public void sendMessage(View view){
    	db.deleteAll();
    	// Do something in response to button
    Intent intent = new Intent(this, DisplayMessageActivity.class);
    
    EditText ed =(EditText) findViewById(R.id.edit_org);
    ed.setFocusable(true);
    
    EditText editOrg = (EditText) findViewById(R.id.edit_org);
    EditText editName = (EditText) findViewById(R.id.edit_name);
    EditText editDate = (EditText) findViewById(R.id.edit_date);
    EditText editTime = (EditText) findViewById(R.id.edit_time);
    EditText editLocation = (EditText) findViewById(R.id.edit_location);
    EditText editDetail = (EditText) findViewById(R.id.edit_detail);
    EditText editFree = (EditText) findViewById(R.id.edit_free);
    EditText editPhone = (EditText) findViewById(R.id.edit_phone);
    EditText editEmail = (EditText) findViewById(R.id.edit_email);
    EditText editLink = (EditText) findViewById(R.id.edit_link);
    String org = editOrg.getText().toString();
    String name = editName.getText().toString();
    String date = editDate.getText().toString();
    String time = editTime.getText().toString();
    String location = editLocation.getText().toString();
    String detail = editDetail.getText().toString();
    String free = editFree.getText().toString();
    String phone = editPhone.getText().toString();
    String email = editEmail.getText().toString();
    String link = editLink.getText().toString();
    
   /* String total = ("Org Name is: " + org + "\n" + 
			"Event Name is " + name + "\n" +
			"Date: " + date + "\n" + 
			"Time " + time + "\n" +
			"Location of Event is " + location + "\n" + 
			"Description of Event: " + detail + "\n" +
			"Is if Free? " + free + "\n" + 
			"Contact Number is: " + phone + "\n" +
			"Contact Email is:  " + email + "\n" + 
			"Link for Event: " + link + "\n" );
			*/
    
    
    intent.putExtra(EXTRA_ORG, org);
    intent.putExtra(EXTRA_NAME, name);
    intent.putExtra(EXTRA_DATE, date);
    intent.putExtra(EXTRA_TIME, time);    
    intent.putExtra(EXTRA_LOCATION, location);
    intent.putExtra(EXTRA_DETAIL, detail);
    intent.putExtra(EXTRA_FREE, free);
    intent.putExtra(EXTRA_PHONE, phone);
    intent.putExtra(EXTRA_EMAIL, email);
    intent.putExtra(EXTRA_LINK, link);
    
    Intent startingPoint = new Intent("com.example.foodfanatics.MAIN");
  	startActivity(startingPoint);		
    String blah = "Thank You for entering information about this event: \n" + name;
  	for (int i=0; i < 2; i++)
  	{
  	      Toast.makeText(this, blah, Toast.LENGTH_LONG).show();
  	}
	// Toast.makeText(getApplicationContext(),
    
    // Toast.LENGTH_SHORT).show();
    
     //startActivity(intent);
    }
    
}
