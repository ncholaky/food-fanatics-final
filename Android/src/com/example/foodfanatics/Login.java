package com.example.foodfanatics;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Login extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	
	
	public void verify(View view){
		 EditText eText  = (EditText)findViewById(R.id.email);
		 String email = eText.getText().toString();
		 Pattern pattern = Pattern.compile("\\S+?@ucsd\\.edu");
		 Matcher matcher = pattern.matcher(email);
		 
		 if (matcher.matches()) {
		      	Intent startingPoint = new Intent("com.example.foodfanatics.MAIN");
		      	startActivity(startingPoint);		   
		}
		 else
		 {
			 eText.setText("Enter a valid UCSD email");
			 eText.setHintTextColor(Color.parseColor("#8C001A"));
		 }
		 }
	}

