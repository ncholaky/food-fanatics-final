package com.example.foodfanatics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.example.dbtest.Events;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class dbHelp extends SQLiteOpenHelper{

	 private static final int DATABASE_VERSION = 1;
	 
	 private static dbHelp dbHelp_instance;
	 
	    // Database Name
	    private static final String DATABASE_NAME = "eventInfo";
	 
	    // Eventss table name
	    private static final String TABLE_EVENTS = "events";	 
	    // Eventss Table Columns names
	    private static final String KEY_ID = "id";
	    private static final String KEY_ORG = "org";
	    private static final String KEY_EVENT_NAME = "event_name";
	    private static final String KEY_EVENT_DATE = "event_date";
	    private static final String KEY_EVENT_TIME = "event_time";
	    private static final String KEY_LOCATION = "location";
	    private static final String KEY_DETAILS = "details";
	    private static final String KEY_FREE = "free"; 
	    private static final String KEY_PH_NO = "phone_number";
	    private static final String KEY_EMAIL = "email";
	    private static final String KEY_LINK = "link";
	 
	    private dbHelp(Context context) {
	        super(context, DATABASE_NAME, null, DATABASE_VERSION);
	    }
	    
	    public static dbHelp getInstance(Context context) {
	    	if(dbHelp_instance == null) {
	    		dbHelp_instance = new dbHelp(context);
	    	}
	    	
	    	return dbHelp_instance;	
	    }
	 
	    // Creating Tables
	    @Override
	    public void onCreate(SQLiteDatabase db) {
	        String CREATE_EVENT_TABLE = "CREATE TABLE " + TABLE_EVENTS + "("
	        		+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_ORG + " TEXT,"
	                + KEY_EVENT_NAME + " TEXT," + KEY_EVENT_DATE + " TEXT,"
	                + KEY_EVENT_TIME + " TEXT," + KEY_LOCATION + " TEXT,"
	                + KEY_DETAILS + " TEXT," + KEY_FREE + " TEXT,"
	                + KEY_PH_NO + " TEXT, "
	                + KEY_EMAIL + " TEXT," + KEY_LINK + " TEXT" + ")";
	        db.execSQL(CREATE_EVENT_TABLE);
	    }
	    // Upgrading database
	    @Override
	    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	        // Drop older table if existed
	        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);
	 
	        // Create tables again
	        onCreate(db);
	    }	  
	    // Adding new event
	public void addEvents(JSONObject event) throws JSONException {
	    SQLiteDatabase db = this.getWritableDatabase();	 
	    ContentValues values = new ContentValues();
	  //  if (db.get event.getInt("id") )
	    values.put(KEY_ID,  event.getInt("id"));
	    values.put(KEY_ORG, event.getString("org")); // Org or user
	    values.put(KEY_EVENT_NAME, event.getString("event_name")); // Name
	    values.put(KEY_EVENT_DATE, event.getString("event_date"));
	    values.put(KEY_EVENT_TIME, event.getString("event_time"));	    
	    values.put(KEY_LOCATION, event.getString("location"));	    
	    values.put(KEY_DETAILS, event.getString("details"));
	    values.put(KEY_FREE, event.getString("free"));
	    values.put(KEY_PH_NO, event.getString("phone_number")); // Phone Number
	    values.put(KEY_EMAIL, event.getString("email"));
	    values.put(KEY_LINK, event.getString("link"));
	    
	    // Inserting Row8
	    db.insert(TABLE_EVENTS, null, values);
	    db.close(); // Closing database connection
	}
	
	
	public String Exist(String id) {        
	     String username="";
	     SQLiteDatabase db = this.getReadableDatabase();    
	     
	     try { 
	         Cursor c = db.query(TABLE_EVENTS, null, KEY_ID + "=?", new String[]{String.valueOf(id)},null, null, null);                                               
	         if (c == null) {                        
	             return username;                                   
	         }
	         else {    
	             c.moveToFirst();               
	             username = c.getString(c.getColumnIndex(KEY_ID)); 
	         }                           
	     }
	     catch(Exception e){
	         e.printStackTrace();
	     }
	     return username; 
	}


    // Deleting single event
	public void deleteEvent(Events event) {
	    SQLiteDatabase db = this.getWritableDatabase();
	    db.delete(TABLE_EVENTS, KEY_ID + " = ?",
	            new String[] {String.valueOf(event.getID()) });
	    db.close();
	}
		
	public void deleteAll()
	{
		String selectQuery = "DELETE FROM " + TABLE_EVENTS ;
		SQLiteDatabase db = this.getWritableDatabase();
		db.rawQuery(selectQuery, null);
	}
	
	public List<Events> getAllEvents() {		
	     List<Events> EventsList = new ArrayList<Events>();
	        // Select All Query
	        String selectQuery = "SELECT  * FROM " + TABLE_EVENTS + " ORDER BY " + KEY_EVENT_DATE + " DESC";
	 
	        SQLiteDatabase db = this.getWritableDatabase();
	        Cursor cursor = db.rawQuery(selectQuery, null);
	 
	        // looping through all rows and adding to list
	        if (cursor.moveToFirst()) {
	            do {
	                Events Events = new Events();
	                //Events.setID(Integer.parseInt(cursor.getString(0)));
	                Events.setName(cursor.getString(1));
	                Events.setDate(cursor.getString(2));
	                Events.setTime(cursor.getString(3));
	                Events.setLocation(cursor.getString(4));
	                Events.setDetails(cursor.getString(5));//was 4
	                Events.setFree(cursor.getString(6));
	                Events.setPhoneNumber(cursor.getString(7));
	                Events.setEmail(cursor.getString(8));
	                Events.setLink(cursor.getString(9));
	                Events.setOrg(cursor.getString(0));
	                // Adding Events to list
	                EventsList.add(Events);
	            } while (cursor.moveToNext());
	        }
	 
	        // return Events list
	        return EventsList;		
	}
	
	  // Getting Event Count
    public int getEventCount() {
        String countQuery = "SELECT  * FROM " + TABLE_EVENTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
 
        // return count
        return cursor.getCount();
    }
	  
	 
}