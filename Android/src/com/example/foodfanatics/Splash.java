package com.example.foodfanatics;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class Splash extends Activity {

	@Override
	protected void onCreate(Bundle hellofrenz) {
		// TODO Auto-generated method stub
		super.onCreate(hellofrenz);
		setContentView(R.layout.splash);

		Thread timer = new Thread() {
			public void run() {
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					Intent startingPoint = new Intent(
							"com.example.foodfanatics.LOGIN");
					startActivity(startingPoint);
					//setContentView(R.layout.login); ///////////////////////////Narine
				}
			}
		};
		timer.start();
	}
}
