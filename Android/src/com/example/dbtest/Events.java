package com.example.dbtest;


public class Events{

	
	//private variables
    int _id;
    String _event_name;
    String _event_date;
    String _event_time;
    String _location;
    String _details;
    String _free;
    String _phone_number;
    String _org;
    String _email;
    String _link; 
    
     
    // Empty constructor
    public Events(){
    
    }
    // constructor
    	public Events(int id, String org, String name, String date, String time,
    		String location, String details, String free, String _phone_number, 
    		String email, String link){
        this._id = id;
        this._event_name = name;
        this._event_date = date;
        this._event_time = time;
        this._location = location;
        this._details = details;
        this._free = free;
        this._phone_number = _phone_number;
        this._org = org;
        this._email = email;
        this._link = link;
    }
    
    
    	
       // constructor
      	public Events(int id, String [] param){
    	        this._id = id;
    	        this._org = param[0];
    	        this._event_name = param[1];
    	        this._event_date = param[2];
    	        this._event_time = param[3];
    	        this._location = param[4];
    	        this._details = param[5];
    	        this._free = param[6];
    	        this._phone_number = param[7];    	        
    	        this._email = param[8];
    	        this._link = param[9];
    	    }
    	    	
    	    	 	
    	
     
    // constructor
    public Events(String name, String _phone_number){
        this._event_name = name;
        this._phone_number = _phone_number;
    }
    // getting ID
    public int getID(){
        return this._id;
    }
     
    // setting id
    public void setID(int id){
        this._id = id;
    }
    
    // getting name
    public String getName(){
        return this._event_name;
    }
    // setting name
    public void setName(String name){
        this._event_name = name;
    }
     
    // setting date
    public void setDate(String date){
        this._event_date = date;
    }
    // getting date
    public String getDate(){
        return this._event_date;
    }
    // getting time
    public String getTime(){
        return this._event_time;
    }
     
    // setting time
    public void setTime(String time){
        this._event_time = time;
    }
    
    // getting location
    public String getLocation(){
        return this._location;
    }
     
    // setting location
    public void setLocation(String location){
        this._location = location;
    }
     
    // getting details
    public String getDetails(){
        return this._details;
    }
     
    // setting details
    public void setDetails(String details){
        this._details = details;
    }         
  
     
    // getting phone number
    public String getPhoneNumber(){
        return this._phone_number;
    }
     
    // setting phone number
    public void setPhoneNumber(String phone_number){
        this._phone_number = phone_number;
    }
    
    // getting org
    public String getOrg(){
        return this._org;
    }
     
    // setting org
    public void setOrg(String org){
        this._org = org;
    }
    
    // getting email
    public String getEmail(){
        return this._email;
    }
     
    // setting email
    public void setEmail(String email){
        this._email = email;
    }

    // getting link
    public String getLink(){
        return this._link;
    }
     
    // setting link
    public void setLink(String link){
        this._link = link;
    }
    
    public String getFree()
    
    {
    	return this._free;
    }
    
    public void setFree(String free)
    {
    	this._free = free;
    }
}
