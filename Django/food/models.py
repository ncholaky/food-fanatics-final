from django.db import models

class Event(models.Model):
	org = models.CharField(max_length=200)
	name = models.CharField(max_length=200)
	date = models.DateTimeField('date published')
	location = models.CharField(max_length=200)
	details = models.TextField()

	def __unicode__(self):  
    		return self.name

